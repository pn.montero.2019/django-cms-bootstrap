from django.apps import AppConfig


class CmsCssBootstrapAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cms_css_bootstrap_app'
