from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about_page, name="about_page"),
    path('<name>', views.page, name='page')
]